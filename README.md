PopularShowsApp

Test project that shows the most popular TV shows using themoviedb API.

MVP Architecture

Libraries

- Retrofit and RxAndroid for Network calls
- Conductor --> Replace fragment
- Lottie --> Airbnb library for json animation
- Epoxy --> Helper library for RecyclerViews
- Glide --> For image donwload and caching
- Butterknife --> For View binding

TODO

- Nice transition between list and detail