package com.ac.popularshowsapp.network;


import com.ac.popularshowsapp.network.models.PopularShowsResponseModel;
import com.ac.popularshowsapp.network.models.SimilarShowsResponseModel;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ShowsAPI {
    @GET("tv/popular")
    Observable<PopularShowsResponseModel> getPopularShows(@Query("api_key") String api_key,
                                                          @Query("language") String language,
                                                          @Query("page") String page);

    @GET("tv/popular")
    Observable<SimilarShowsResponseModel> getSimilarShows(@Query("api_key") String api_key,
                                                    @Query("language") String language,
                                                    @Query("page") String page,
                                                    @Query("tv_id") String showId);
}
