package com.ac.popularshowsapp.features.showdetails;


import com.ac.popularshowsapp.core.BasePresenter;
import com.ac.popularshowsapp.core.BaseView;
import com.ac.popularshowsapp.network.models.ShowModel;

import java.util.List;

public class ShowDetailsContract {
    interface View extends BaseView<Presenter> {
        void setSimilarShows(List<ShowModel> showsList);
        void setListLoading(Boolean active);
        void showError();
        void clearSimilarList();
    }

    interface Presenter extends BasePresenter {
        void getPopularShows(int showId);
    }
}
