package com.ac.popularshowsapp.features.showslist;


import android.content.Context;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ac.popularshowsapp.R;
import com.airbnb.epoxy.CallbackProp;
import com.airbnb.epoxy.ModelProp;
import com.airbnb.epoxy.ModelView;
import com.airbnb.epoxy.TextProp;
import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import butterknife.BindView;
import butterknife.ButterKnife;

@ModelView(autoLayout = ModelView.Size.MATCH_WIDTH_WRAP_HEIGHT)
public class PopularShowsListItem extends ConstraintLayout {
    private static final String IMAGE_URL = "https://flagpedia.net/data/flags/small/";
    private static final String IMAGE_EXT = ".png";

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.img_poster)
    ImageView imgPoster;
    @BindView(R.id.img_flag)
    ImageView imgFlag;
    @BindView(R.id.av_star)
    LottieAnimationView avStar;
    @BindView(R.id.tv_rating)
    TextView tvRating;
    @BindView(R.id.tv_first_air_date)
    TextView tvFirstAirDate;

    public PopularShowsListItem(Context context) {
        super(context);
        initialize();
    }

    private void initialize() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.item_popular_shows, this, true);
        ButterKnife.bind(this, view);
        avStar.setOnClickListener( c -> {
            avStar.playAnimation();
        });
    }

    @TextProp
    void setTitle(CharSequence text) {
        tvTitle.setText(text);
    }

    @TextProp
    void setRating(CharSequence text) {
        tvRating.setText(text);
    }

    @TextProp
    void setFirstAirDate(CharSequence text) {
        tvFirstAirDate.setText(text);
    }

    @TextProp
    void setImagePoster(CharSequence imagePath) {
        Glide
                .with(this)
                .asBitmap()
                .load("https://image.tmdb.org/t/p/w154/"+imagePath)
                .apply(new RequestOptions().placeholder(R.drawable.ic_poster_placeholder))
                .into(imgPoster);
    }

    @TextProp
    void setFlag(CharSequence countryCode) {
        Glide
                .with(this)
                .asBitmap()
                .load(IMAGE_URL+countryCode+IMAGE_EXT)
                .into(imgFlag);
    }

    @ModelProp
    public void setStarred(boolean starred) {
        avStar.cancelAnimation();
        avStar.setProgress(0f);
    }

    @CallbackProp // Use this annotation for click listeners or other callbacks.
    public void clickListener(@Nullable OnClickListener listener) {
        this.setOnClickListener(listener);
    }
}
