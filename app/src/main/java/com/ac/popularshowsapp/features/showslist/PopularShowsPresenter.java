package com.ac.popularshowsapp.features.showslist;

import android.support.annotation.NonNull;
import android.util.Log;

import com.ac.popularshowsapp.BuildConfig;
import com.ac.popularshowsapp.network.ShowsAPIService;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class PopularShowsPresenter implements PopularShowsContract.Presenter {

    @NonNull
    private CompositeDisposable compositeDisposable;

    private PopularShowsContract.View view;

    public PopularShowsPresenter(PopularShowsContract.View view) {
        this.view = view;
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void start() {
        view.clearList();
        loadMoreShows(0);
    }

    @Override
    public void stop() {
        compositeDisposable.clear();
    }

    @Override
    public void loadMoreShows(int page) {
        view.setLoadingIndicator(true);
        Disposable disposable = ShowsAPIService.getShowsAPIClient()
                .getPopularShows(BuildConfig.API_KEY, "en-US", String.valueOf(page+1))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        popularShowsResponseModel -> view.showPopularShows(popularShowsResponseModel.getResults()),
                        error -> {
                            //TODO Handle error
                            Log.d("123","123");
                        },
                        () -> {
                          view.setLoadingIndicator(false);
                        }
                );

        compositeDisposable.add(disposable);
    }
}
