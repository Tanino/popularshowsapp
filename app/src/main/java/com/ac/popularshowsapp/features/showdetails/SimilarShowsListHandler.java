package com.ac.popularshowsapp.features.showdetails;


import com.ac.popularshowsapp.features.showslist.PopularShowsListItemModel_;
import com.ac.popularshowsapp.features.showslist.PopularShowsListLoaderCellModel_;
import com.ac.popularshowsapp.network.models.ShowModel;
import com.airbnb.epoxy.EpoxyController;

import java.util.ArrayList;
import java.util.List;

public class SimilarShowsListHandler extends EpoxyController {
    private static String LOADER_ID = "LOADER_ID";
    private boolean loading;
    private List<ShowModel> showsList = new ArrayList<>();
    private PopularShowsListLoaderCellModel_ loader = new PopularShowsListLoaderCellModel_();

    public SimilarShowsListHandler(SimilarShowsListListener listener) {
        this.listener = listener;
    }

    public interface SimilarShowsListListener {
        void onItemClick(ShowModel model);
    }

    private SimilarShowsListListener listener;

    @Override
    protected void buildModels() {
        loader.id(LOADER_ID).addIf(loading,this);
        for ( ShowModel model : showsList) {
            new SimilarShowListItemModel_()
                    .id(model.getId())
                    .title(model.getName())
                    .imagePoster(model.getPosterPath() == null ? "" : model.getPosterPath())
                    .clickListener( view -> {
                        listener.onItemClick(model);
                    })
                    .addTo(this);
        }
    }

    public void setLoading(boolean loading) {
        this.loading = loading;
    }

    public void addToList(List<ShowModel> showsList) {
        this.showsList.addAll(showsList);
    }

    public void clearList() {
        showsList.clear();
    }
}
