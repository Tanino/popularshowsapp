package com.ac.popularshowsapp.features.showdetails;


import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.ac.popularshowsapp.BuildConfig;
import com.ac.popularshowsapp.network.ShowsAPIService;
import com.ac.popularshowsapp.network.models.ShowModel;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ShowDetailsPresenter implements ShowDetailsContract.Presenter {

    @NonNull
    private CompositeDisposable compositeDisposable;

    private ShowDetailsContract.View view;
    private ShowModel model;

    public ShowDetailsPresenter(ShowDetailsContract.View view, ShowModel model) {
        this.view = view;
        this.model = model;
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void start() {
        view.clearSimilarList();
        getPopularShows(model.id);
    }

    @Override
    public void stop() {
        compositeDisposable.clear();
    }

    @Override
    public void getPopularShows(int showId) {
        view.setListLoading(true);
        Disposable disposable = ShowsAPIService.getShowsAPIClient()
                .getSimilarShows(BuildConfig.API_KEY, "en-US", String.valueOf(1), String.valueOf(showId))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        similarShowsResponseModel -> view.setSimilarShows(similarShowsResponseModel.getResults()),
                        error -> {
                            view.showError();
                        },
                        () -> {
                            view.setListLoading(false);
                        }
                );

        compositeDisposable.add(disposable);
    }
}
