package com.ac.popularshowsapp.features.showslist;


import com.ac.popularshowsapp.network.models.ShowModel;
import com.airbnb.epoxy.EpoxyController;

import java.util.ArrayList;
import java.util.List;

public class PopularShowsListHandler extends EpoxyController {
    private static String LOADER_ID = "LOADER_ID";
    private List<ShowModel> showsList = new ArrayList<>();
    private PopularShowsListLoaderCellModel_ loader = new PopularShowsListLoaderCellModel_();

    private boolean loading;

    public PopularShowsListHandler(PopularShowsListListener listener) {
        this.listener = listener;
    }

    private PopularShowsListListener listener;

    public interface PopularShowsListListener {
        void onItemClick(ShowModel model);
    }

    @Override
    protected void buildModels() {
        for ( ShowModel model : showsList) {
            new PopularShowsListItemModel_()
                    .id(model.getId())
                    .title(model.getName())
                    .rating(String.valueOf(model.getVoteAverage()))
                    .firstAirDate(model.getFirstAirDate())
                    .flag(model.getOriginCountry().isEmpty() ? "" : model.getOriginCountry().get(0).toLowerCase())
                    .imagePoster(model.getPosterPath() == null ? "" : model.getPosterPath())
                    .starred(false)
                    .clickListener( view -> {
                        listener.onItemClick(model);
                    })
                    .addTo(this);
        }
        loader.id(LOADER_ID).addIf(loading,this);
    }

    void addToList(List<ShowModel> shows) {
        showsList.addAll(shows);
    }

    public boolean isLoading() {
        return loading;
    }

    public void setLoading(boolean loading) {
        this.loading = loading;
    }

    public void clearList(){
        this.showsList.clear();
    }
}
