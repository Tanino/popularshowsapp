package com.ac.popularshowsapp.features.showslist;


import com.ac.popularshowsapp.core.BasePresenter;
import com.ac.popularshowsapp.core.BaseView;
import com.ac.popularshowsapp.network.models.ShowModel;

import java.util.List;

public interface PopularShowsContract {

    interface View extends BaseView<Presenter> {
        void setLoadingIndicator(boolean active);
        void showPopularShows(List<ShowModel> shows);
        void clearList();
    }

    interface Presenter extends BasePresenter {
        void loadMoreShows(int page);
    }

}
