package com.ac.popularshowsapp.features.showslist;


import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import com.ac.popularshowsapp.R;
import com.airbnb.epoxy.ModelView;

import butterknife.ButterKnife;

@ModelView(autoLayout = ModelView.Size.MATCH_WIDTH_WRAP_HEIGHT)
public class PopularShowsListLoaderCell extends FrameLayout {
    public PopularShowsListLoaderCell(@NonNull Context context) {
        super(context);
        initialize();
    }

    private void initialize() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.item_loader_popular_shows, this, true);
        ButterKnife.bind(this, view);
    }
}
