package com.ac.popularshowsapp.features.showdetails;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ac.popularshowsapp.BuildConfig;
import com.ac.popularshowsapp.MainActivity;
import com.ac.popularshowsapp.R;
import com.ac.popularshowsapp.core.BaseController;
import com.ac.popularshowsapp.network.models.ShowModel;
import com.ac.popularshowsapp.utils.BundleBuilder;
import com.bluelinelabs.conductor.RouterTransaction;
import com.bluelinelabs.conductor.changehandler.FadeChangeHandler;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import org.parceler.Parcels;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ShowDetailsController extends BaseController implements ShowDetailsContract.View, SimilarShowsListHandler.SimilarShowsListListener {

    private static final String KEY_SHOW = "KEY_SHOW";
    private static final String POSTER_RES_URL = "/w154/";
    private static final String BACKDROP_RES_URL = "/w1280/";

    private SimilarShowsListHandler listHandler;

    @BindView(R.id.img_banner)
    ImageView imgBanner;
    @BindView(R.id.img_poster)
    ImageView imgPoster;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_rating)
    TextView tvRating;
    @BindView(R.id.tv_rating_title)
    TextView tvRatingTitle;
    @BindView(R.id.tv_vote_count)
    TextView tvVoteCount;
    @BindView(R.id.tv_overview)
    TextView tvOverview;
    @BindView(R.id.rv_similar)
    RecyclerView rvSimilar;

    private ShowDetailsContract.Presenter presenter;
    private ShowModel model;

    public ShowDetailsController(Bundle args) {
        super(args);
        model = Parcels.unwrap(args.getParcelable(KEY_SHOW));
    }

    public ShowDetailsController(ShowModel show) {
        this(new BundleBuilder(new Bundle())
                .putParcelable(KEY_SHOW, Parcels.wrap(show))
                .build());
    }

    @NonNull
    @Override
    protected View onCreateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container) {
        View view = inflater.inflate(R.layout.controller_show_details, container, false);
        ButterKnife.bind(this, view);
        setupPresenter();
        setActive(true);
        populateView();
        return view;
    }

    private void populateView() {
        setBannerImage();
        setPosterImage();
        setTitle();
        setRating();
        setVoteCount();
        setOverview();
        setupRecyclerView();
    }

    private void setupRecyclerView() {
        rvSimilar.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false));
        listHandler = new SimilarShowsListHandler(this);
        rvSimilar.setAdapter(listHandler.getAdapter());
        listHandler.requestModelBuild();
    }

    private void setOverview() {
        tvOverview.setText(model.getOverview());
    }

    private void setVoteCount() {
        tvVoteCount.setText(String.valueOf(model.getVoteCount()));
    }

    private void setRating() {
        tvRating.setText(String.valueOf(model.getVoteAverage()));
    }

    private void setTitle() {
        tvTitle.setText(String.valueOf(model.getName()));
    }

    @Override
    protected void onAttach(@NonNull View view) {
        super.onAttach(view);
        presenter.start();
    }

    private void setPosterImage() {
        Glide
                .with(getApplicationContext())
                .asBitmap()
                .load(BuildConfig.IMAGE_BASE_URL + POSTER_RES_URL + model.getPosterPath())
                .apply(new RequestOptions().placeholder(R.drawable.ic_poster_placeholder))
                .into(imgPoster);
    }

    private void setBannerImage() {
        Glide
                .with(getApplicationContext())
                .asBitmap()
                .load(BuildConfig.IMAGE_BASE_URL + BACKDROP_RES_URL + model.getBackdropPath())
                .into(imgBanner);
    }

    private void setupPresenter() {
        presenter = new ShowDetailsPresenter(this, model);
    }

    @Override
    public void setSimilarShows(List<ShowModel> showsList) {
        listHandler.addToList(showsList);
        listHandler.requestModelBuild();
    }

    @Override
    public void setListLoading(Boolean active) {
        listHandler.setLoading(active);
        listHandler.requestModelBuild();
    }

    @Override
    public void showError() {
        Toast.makeText(getActivity(), "Cannot get similar shows!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void clearSimilarList() {
        listHandler.clearList();
    }

    @Override
    public void onItemClick(ShowModel model) {
        getRouter().pushController(RouterTransaction.with(new ShowDetailsController(model)).pushChangeHandler(new FadeChangeHandler())
                .popChangeHandler(new FadeChangeHandler()));
    }
}
