package com.ac.popularshowsapp.features.showslist;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ac.popularshowsapp.MainActivity;
import com.ac.popularshowsapp.R;
import com.ac.popularshowsapp.core.BaseController;
import com.ac.popularshowsapp.features.showdetails.ShowDetailsController;
import com.ac.popularshowsapp.network.models.ShowModel;
import com.ac.popularshowsapp.utils.listener.EndlessRecyclerViewScrollListener;
import com.airbnb.epoxy.EpoxyRecyclerView;
import com.bluelinelabs.conductor.RouterTransaction;
import com.bluelinelabs.conductor.changehandler.FadeChangeHandler;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PopularShowsController extends BaseController implements PopularShowsContract.View, PopularShowsListHandler.PopularShowsListListener {

    @BindView(R.id.rv_popularshows)
    EpoxyRecyclerView rvPopularshows;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private PopularShowsContract.Presenter presenter;
    private EndlessRecyclerViewScrollListener scrollListener;
    private PopularShowsListHandler listHandler;

    @NonNull
    @Override
    protected View onCreateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container) {
        View view = inflater.inflate(R.layout.controller_popular_shows, container, false);
        ButterKnife.bind(this, view);
        setupToolbar();
        setupList();
        setupPresenter();
        setActive(true);
        return view;
    }

    private void setupToolbar() {

    }

    private void setupList() {
        scrollListener = new EndlessRecyclerViewScrollListener((LinearLayoutManager)rvPopularshows.getLayoutManager()) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                presenter.loadMoreShows(page);
            }
        };

        rvPopularshows.addOnScrollListener(scrollListener);
        listHandler = new PopularShowsListHandler(this);
        rvPopularshows.setAdapter(listHandler.getAdapter());
        listHandler.requestModelBuild();
    }

    private void setupPresenter() {
        presenter = new PopularShowsPresenter(this);
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        listHandler.setLoading(active);
        listHandler.requestModelBuild();
    }

    @Override
    public void showPopularShows(List<ShowModel> shows) {
        listHandler.addToList(shows);
        listHandler.requestModelBuild();
    }

    @Override
    public void clearList() {
        listHandler.clearList();
    }

    @Override
    protected void onAttach(@NonNull View view) {
        super.onAttach(view);
        presenter.start();
    }

    @Override
    public void onItemClick(ShowModel model) {
        getRouter().pushController(RouterTransaction.with(new ShowDetailsController(model)).pushChangeHandler(new FadeChangeHandler())
                .popChangeHandler(new FadeChangeHandler()));
    }
}
