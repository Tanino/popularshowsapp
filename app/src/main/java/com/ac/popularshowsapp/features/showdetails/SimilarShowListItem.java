package com.ac.popularshowsapp.features.showdetails;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.ac.popularshowsapp.R;
import com.airbnb.epoxy.CallbackProp;
import com.airbnb.epoxy.ModelView;
import com.airbnb.epoxy.TextProp;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import butterknife.BindView;
import butterknife.ButterKnife;

@ModelView(autoLayout = ModelView.Size.WRAP_WIDTH_WRAP_HEIGHT)
public class SimilarShowListItem extends FrameLayout {
    @BindView(R.id.img_poster)
    ImageView imgPoster;
    @BindView(R.id.tv_title)
    TextView tvTitle;

    public SimilarShowListItem(@NonNull Context context) {
        super(context);
        initialize();
    }

    private void initialize() {
        this.setPadding(15,10,15,10);
        this.setClipToPadding(false);
        View view = LayoutInflater.from(getContext()).inflate(R.layout.item_similar_show, this, true);
        ButterKnife.bind(this, view);
    }

    @TextProp
    void setTitle(CharSequence text) {
        tvTitle.setText(text);
    }

    @TextProp
    void setImagePoster(CharSequence imagePath) {
        Glide
                .with(this)
                .asBitmap()
                .load("https://image.tmdb.org/t/p/w154/" + imagePath)
                .apply(new RequestOptions().placeholder(R.drawable.ic_poster_placeholder))
                .into(imgPoster);
    }

    @CallbackProp // Use this annotation for click listeners or other callbacks.
    public void clickListener(@Nullable OnClickListener listener) {
        this.setOnClickListener(listener);
    }
}
