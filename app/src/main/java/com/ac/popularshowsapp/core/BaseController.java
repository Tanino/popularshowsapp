package com.ac.popularshowsapp.core;


import android.os.Bundle;

import com.bluelinelabs.conductor.Controller;

public abstract class BaseController extends Controller {
    private boolean isActive = false;

    public BaseController() { }

    public BaseController(Bundle args) {
        super(args);
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
