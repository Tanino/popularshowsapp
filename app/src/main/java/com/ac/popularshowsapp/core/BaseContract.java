package com.ac.popularshowsapp.core;


public interface BaseContract {
    interface BaseView{}
    interface BasePresenter{}
}
