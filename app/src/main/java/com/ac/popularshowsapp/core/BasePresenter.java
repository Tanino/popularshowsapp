package com.ac.popularshowsapp.core;


public interface BasePresenter {
    void start();
    void stop();
}
