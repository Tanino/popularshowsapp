package com.ac.popularshowsapp.common.ui;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.Region;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

;

public class ArcBannerImage  extends android.support.v7.widget.AppCompatImageView{

    private Path clipPath;
    
    public ArcBannerImage(Context context) {
        super(context);
        init();
    }

    public ArcBannerImage(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ArcBannerImage(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        this.clipPath = new Path();
    }

    private void calculatePath(float radius) {
        this.clipPath.reset();
        this.clipPath.moveTo(0, 0);
        this.clipPath.lineTo(0, getMeasuredHeight() - 75);
        this.clipPath.cubicTo(0, getMeasuredHeight() -75, getMeasuredWidth() / 2, getMeasuredHeight(), getMeasuredWidth(), getMeasuredHeight() - 75);
        this.clipPath.lineTo(getMeasuredWidth(), 0);
        this.clipPath.lineTo(0,0);
        this.clipPath.close();
        invalidate();
    }

    @Override
    public void onDraw(Canvas c) {
        c.clipPath(clipPath, Region.Op.INTERSECT);
        c.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        super.onDraw(c);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec){
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);
        setMeasuredDimension(width, height);
        calculatePath(Math.min(width / 2f, height / 2f) - 10f);
    }
}